# Menú
- [Que es git](#Que-es-git)
- [Comandos de git en consola](#Comandos-de-git-en-consola)
- [Clientes git](#Clientes-git)
- [Clonación de proyecto por consola y por cliente](#Clonación-de-proyecto-por-consola-y-por-cliente)
- [Commits por consola y por cliente kraken o smart](#Commits-por-consola-y-por-cliente-kraken-o-smart)
- [Ramas de kraken](#Ramas-de-kraken)
- [Merge](#Merge)

# Que es git
Git es un proyecto de código abierto maduro y con un mantenimiento activo que desarrolló originalmente Linus Torvalds, el famoso creador del kernel del sistema operativo Linux, en 2005. Un asombroso número de proyectos de software dependen de Git para el control de versiones, incluidos proyectos comerciales y de código abierto. 

# Comandos de git en consola 

- git init
    -
    
    <img src="https://gitlab.com/sjv.guerra/capacitaciones-yv-mlab-2021/-/raw/master/Imagenes/git%20init.png" width=350>

- git status

    <img src="https://gitlab.com/sjv.guerra/capacitaciones-yv-mlab-2021/-/raw/master/Imagenes/git%20status.png" width=350>

- git touch

    <img src="https://gitlab.com/sjv.guerra/capacitaciones-yv-mlab-2021/-/raw/master/Imagenes/git%20touch.png" width=350>

- git add

<img src="https://gitlab.com/sjv.guerra/capacitaciones-yv-mlab-2021/-/raw/master/Imagenes/git%20add.png" width=350>



# Clientes git
<img src="https://gitlab.com/sjv.guerra/capacitaciones-yv-mlab-2021/-/raw/master/Imagenes/clientes%20git.png" width=350>

# Clonación de proyecto por consola y por cliente
<img src="https://gitlab.com/sjv.guerra/capacitaciones-yv-mlab-2021/-/raw/master/Imagenes/clonar%20un%20repo.png" width=350>

<img src="https://gitlab.com/sjv.guerra/capacitaciones-yv-mlab-2021/-/raw/master/Imagenes/clonar%20por%20consola.png" width=350>

# Commits por consola y por cliente kraken o smart
<img src="https://gitlab.com/sjv.guerra/capacitaciones-yv-mlab-2021/-/raw/master/Imagenes/git%20commit%20consola.png" width=350>

<img src="https://gitlab.com/sjv.guerra/capacitaciones-yv-mlab-2021/-/raw/master/Imagenes/commit%20kraken.png" width=350>

# Ramas de kraken
<img src="https://gitlab.com/sjv.guerra/capacitaciones-yv-mlab-2021/-/raw/master/Imagenes/Rama.png" width=350> 

# Merge
<img src="https://gitlab.com/sjv.guerra/capacitaciones-yv-mlab-2021/-/raw/master/Imagenes/merge.png" width=350>


<img src="https://gitlab.com/sjv.guerra/capacitaciones-yv-mlab-2021/-/raw/master/Imagenes/merge%202.png" width=350>