# Menú
- [Información de como acceder](#Información-de-como-acceder)
- [Crear repositorios](#Crear-repositorios)
- [Crear grupos](#Crear-grupos)
- [Crear subgrupos](#Crear-subgrupos)
- [Crear issues](#Crear-issues)
- [Crear labels](#Crear-labels)
- [Roles que cumplen](#Roles-que-cumplen)
- [Agregar miembros](#Agregar-miembros)
- [Crear boards y manejo de boards](#Crear-boards-y-manejo-de-boards)

# Información de como acceder
- Abrir su navegador predeterminado.
- Acceder a la siguiente pagina web: https://about.gitlab.com/
- Entrar en la pagina web 
- Iniciar sesion o crear una nueva cuenta.

<img src="https://gitlab.com/sjv.guerra/capacitaciones-yv-mlab-2021/-/raw/master/Imagenes/inicio%20git.png" width=350>

# Crear repositorios
-Para crear repositorios te vas a proyectos, creas un nuevo proyecto (al momento de crear seleccionar la opcion README y de preferencia que sea publico)
- Una vez creado el proyecto seleccionar la opcion repositorio.

<img src="https://gitlab.com/sjv.guerra/capacitaciones-yv-mlab-2021/-/raw/master/Imagenes/repo%20gitlab.png" width=350>

# Crear grupos
- Un grupo puede tener muchos subgrupos dentro de él y, al mismo tiempo, un grupo puede tener solo un grupo principal inmediato. Se parece a un comportamiento de directorio o una lista de elementos anidados:

- Grupo 1
- Grupo 1.1
- Grupo 1.2
- Grupo 1.2.1
- Grupo 1.2.2
- Grupo 1.2.2.1

<img src="https://gitlab.com/sjv.guerra/capacitaciones-yv-mlab-2021/-/raw/master/Imagenes/grupos%20gitlab.png" width=350>

# Crear subgrupos

- GitLab admite hasta 20 niveles de subgrupos, también conocidos como grupos anidados o grupos jerárquicos.

- Al usar subgrupos, puede hacer lo siguiente:

- Organizaciones internas / externas separadas. Dado que cada grupo puede tener su propio nivel de visibilidad ( público, interno o privado ), puede alojar grupos para diferentes propósitos bajo el mismo paraguas.
- Organiza grandes proyectos. Para proyectos grandes, los subgrupos facilitan potencialmente la separación de permisos en partes del código fuente.
- Facilite la gestión de personas y el control de la visibilidad. Otorgue a las personas diferentes permisos según la membresía de su grupo .
- Para obtener más información sobre los permisos permitidos en grupos y proyectos, consulte los niveles de visibilidad .

<img src="https://gitlab.com/sjv.guerra/capacitaciones-yv-mlab-2021/-/raw/master/Imagenes/subgrupos%20gitlab.png" width=350>

# Crear issues

- Primero que nada nos vamos al GITLAB y creamos un repositorio.
- Apenas tengamos creado el repositorio, nos vamos a nuestra consola de preferencia y realizamos la subida del proyecto angular que hemos creado con anterioridad, tenemos que ejecutar estos comandos git para empezar.
- Con el comando git push origin master, subiremos el proyecto al repositorio.
- Luego de esto, nos vamos al gitlab en el sidebar lateral izquierdo seleccionamos la opción Issue -> List y presionamos New Issue.

<img src="https://gitlab.com/sjv.guerra/capacitaciones-yv-mlab-2021/-/raw/master/Imagenes/issues%20gitlab.png" width=350>

# Crear labels
 - Acceder al repositorio

- Acceder a Repositorio -> Tags  o Etiquetas

- Crear una nueva Etiqueta con los datos completos que nos interesen

- Al guardar, todos los campos completos, en unos minutos vamos a ver dentro de Proyecto -> Versiones la versión para descargar en diferentes formatos y la posibilidad de descargar el código fuente del repositorio al momento de generar la etiqueta.

<img src="https://gitlab.com/sjv.guerra/capacitaciones-yv-mlab-2021/-/raw/master/Imagenes/labels%20gitlab.png" width=350>

# Roles que cumplen
- GitWeb es muy simple. Si buscas un servidor Git más moderno, con todas las funciones, tienes algunas soluciones de código abierto que puedes utilizar en su lugar. Puesto que GitLab es una de las más populares, vamos a ver aquí cómo se instala y se usa, a modo de ejemplo. Es algo más complejo que GitWeb y requiere algo más de mantenimiento, pero es una opción con muchas más funciones.
- La interfaz de administración de GitLab se accede mediante la web. Simplemente abre en tu navegador la IP o el nombre de máquina donde has instalado Gitlab, y entra con el usuario administrador. El usuario predeterminado es admin@local.host, con la contraseña 5iveL!fe (que te pedirá cambiar cuando entres por primera vez). Una vez dentro, pulsa en el icono “Admin area” del menú superior derecho.
- Los usuarios en Gitlab son las cuentas que abre la gente. Las cuentas de usuario no tienen ninguna complicación: viene a ser una colección de información personal unida a la información de login. Cada cuenta tiene un espacio de nombres (namespace) que es una agrupación lógica de los proyectos que pertenecen al usuario. De este modo, si el usuario jane tiene un proyecto llamado project, la URL de ese proyecto sería http://server/jane/project.
- Un grupo de GitLab es un conjunto de proyectos, junto con los datos acerca de los usuarios que tienen acceso. Cada grupo tiene también un espacio de nombres específico (al igual que los usuarios). Por ejemplo, si el grupo formacion tuviese un proyecto materiales su URL sería: http://server/formacion/materiales.
- Un proyecto en GitLab corresponde con un repositorio Git. Cada proyecto pertenece a un espacio de nombres, bien sea de usuario o de grupo. Si el proyecto pertenece a un usuario, el propietario del mismo tendrá control directo sobre quién tiene acceso al proyecto; si el proyecto pertenece a un grupo, los permisos de acceso por parte de los usuarios estarán también determinados por los niveles de acceso de los miembros del grupo.
- GitLab tiene soporte para los enganches (hooks), tanto a nivel de proyecto como del sistema. Para cualquiera de ellos, el servidor GitLab realizará una petición HTTP POST con determinados datos JSON cuando ocurran ciertos eventos. Es una manera interesante de conectar los repositorios y la instancia de GitLab con el resto de los mecanismos automáticos de desarrollo, como servidores de integración continua (CI), salas de charla y otras utilidades de despliegue.
- Lo primero que tienes que hacer en GitLab es crear un nuevo proyecto. Esto lo consigues pulsando el icono “+” en la barra superior. Te preguntará por el nombre del proyecto, el espacio de nombres al que pertenece y qué nivel de visibilidad debe tener. Esta información, en su mayoría, no es fija y puedes cambiarla más tarde en la pantalla de ajustes. Pulsa en “Create Project” y habrás terminado.

- Una vez que tengas el proyecto, querrás usarlo para un repositorio local de Git. Cada proyecto se puede acceder por HTTPS o SSH, protocolos que podemos configurar en nuestro repositorio como un Git remoto. La URL la encontrarás al principio de la página principal del proyecto. Para un repositorio local existente, puedes crear un remoto llamado gitlab

# Agregar miembros
-  inicie sesion en su cuenta de GitLab y navegue hasta su proyecto en Proyectos.
- Luego haga clic en la opcion Miembros debajo de la pestana Configuracion.
- Esto abrira la pantalla de abajo para agregar el miembro a su proyecto.
- Ahora ingrese el nombre d 'usuario, permiso de funcion, fecha de vencimiento (opcional) y haga clic en el boton Agregar al proyecto para agregar un usuario al proyecto.
- luego recibira un mensaje exitoso despues de agregar el usuario al proyecto.
- Tambien puede agregar un usuario al proyecto haciendo clic en el boton Importar.
- Ahora seleccione el proyecto desde el cual desea agregar el usuario para su proyecto y haga clic en el boton Importar miembros del proyecto.
- Recibira su mensaje de exito despues de importar el usuario al proyecto.

<img src="https://gitlab.com/sjv.guerra/capacitaciones-yv-mlab-2021/-/raw/master/Imagenes/miembros%20gitlab.png" width=350>

# Crear boards y manejo de boards
 - Haga clic en el menú desplegable con el nombre del tablero actual en la esquina superior izquierda de la página Tableros de temas.
- Haz clic en Crear tablero nuevo .
- Ingrese el nombre de la nueva placa y seleccione su alcance: hito, etiquetas, cesionario o peso.

<img src="https://gitlab.com/sjv.guerra/capacitaciones-yv-mlab-2021/-/raw/master/Imagenes/boards%20gitlab.png" width=350>